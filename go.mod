module boilerplate

go 1.21.6

require (
	github.com/ettle/strcase v0.2.0
	github.com/iancoleman/strcase v0.3.0
	github.com/manifoldco/promptui v0.9.0
	github.com/spf13/cobra v1.8.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20181122145206-62eef0e2fa9b // indirect
)
