package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/ettle/strcase"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

type manifestField struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`
}

type manifest struct {
	Fields []manifestField `yaml:"fields"`
}

func main() {
	ex, _ := os.Executable()

	templatesPath, _ := os.Getwd()
	outputPath := "."

	cmdRoot := &cobra.Command{
		Use:   "boilerplate",
		Short: "Code bolerplate",
	}

	cmdRoot.AddCommand(
		listCommand(&templatesPath),
		runCommand(&templatesPath, &outputPath),
	)

	cmdRoot.PersistentFlags().StringVarP(&templatesPath, "template-path", "t", filepath.Join(filepath.Dir(ex), "templates"), "template path")
	cmdRoot.PersistentFlags().StringVarP(&outputPath, "toutput-path", "o", outputPath, "output path")

	cmdRoot.Execute()
}

func listCommand(templatesPath *string) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "List templates",
		RunE: func(cmd *cobra.Command, args []string) error {
			entries, err := os.ReadDir(*templatesPath)
			if err != nil {
				return err
			}

			for _, entry := range entries {
				cmd.Println(entry.Name())

				manifestData, err := getManifest(filepath.Join(*templatesPath, entry.Name()))
				if err != nil {
					return err
				}

				for _, field := range manifestData.Fields {
					cmd.Printf("* %s [%s]\n", field.Label, field.Name)
				}
			}

			return nil
		},
	}
}

func runCommand(templatesPath *string, outputPath *string) *cobra.Command {
	funcMap := template.FuncMap{
		"ToUpper":      strings.ToUpper,
		"ToLower":      strings.ToLower,
		"ToSnake":      strcase.ToSnake,
		"ToKebab":      strcase.ToKebab,
		"ToCamel":      strcase.ToCamel,
		"ToGoCamel":    strcase.ToGoCamel,
		"ToGoKebab":    strcase.ToGoKebab,
		"ToGoPascal":   strcase.ToGoPascal,
		"ToGoSnake":    strcase.ToGoSnake,
		"ToUpperKebab": strcase.ToKEBAB,
		"ToUpperSnake": strcase.ToSNAKE,
		"ToPascal":     strcase.ToPascal,
	}

	output := &cobra.Command{
		Use:   "run <template name>",
		Args:  cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
		Short: "Run a template",
		RunE: func(cmd *cobra.Command, args []string) error {

			templatePath := filepath.Join(*templatesPath, args[0])

			manifestData, err := getManifest(templatePath)
			if err != nil {
				return err
			}

			inputData, err := manifestData.Prompt()
			if err != nil {
				return err
			}

			if err := filepath.Walk(templatePath, func(path string, info fs.FileInfo, err error) error {
				if path == templatePath {
					return nil
				}

				if path == filepath.Join(templatePath, "template.yaml") {
					return nil
				}

				outputFilepath := filepath.Join(*outputPath, path[len(templatePath)+1:])

				outputPathName, err := template.New("filename").Funcs(funcMap).Parse(outputFilepath)
				if err != nil {
					return err
				}

				outputPathNameValue := bytes.NewBuffer(nil)
				outputPathName.Execute(outputPathNameValue, inputData)

				if strings.HasSuffix(outputPathNameValue.String(), ".tmpl") {
					val := outputPathNameValue.String()[:len(outputPathNameValue.String())-5]
					outputPathNameValue.Reset()
					outputPathNameValue.WriteString(val)
				}

				if info.IsDir() {
					cmd.Printf(" * [DIR] %s\n", outputPathNameValue.String())
					return os.MkdirAll(outputPathNameValue.String(), os.ModePerm)
				}

				datafile, err := os.ReadFile(path)
				if err != nil {
					return err
				}

				outputData, err := template.New("data").Funcs(funcMap).Parse(string(datafile))
				if err != nil {
					return err
				}

				file, err := os.Create(outputPathNameValue.String())
				if err != nil {
					return err
				}

				defer func() {
					_ = file.Close()
				}()

				if err := outputData.Execute(file, inputData); err != nil {
					return err
				}

				cmd.Printf(" * [FILE] %s\n", outputPathNameValue.String())

				return nil
			}); err != nil {
				return err
			}

			return nil
		},
	}

	return output
}

func getManifest(templatePath string) (*manifest, error) {
	manifestData := manifest{}

	manifestFilename := filepath.Join(templatePath, "template.yaml")

	manifestFile, err := os.Open(manifestFilename)
	if err != nil {
		return nil, fmt.Errorf("wrong '%s' description file", manifestFilename)
	}

	defer func() {
		_ = manifestFile.Close()
	}()

	yaml.NewDecoder(manifestFile).Decode(&manifestData)

	return &manifestData, nil
}

func (m manifest) Prompt() (map[string]any, error) {
	output := map[string]any{}
	for idx := range m.Fields {
		label := m.Fields[idx].Name

		if m.Fields[idx].Label != "" {
			label = m.Fields[idx].Label
		}

		prompt := promptui.Prompt{
			Label: label,
		}

		result, err := prompt.Run()
		if err != nil {
			return nil, err
		}

		output[m.Fields[idx].Name] = result
	}

	return output, nil
}
