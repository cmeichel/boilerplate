package models

import (
	"net/url"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

// {{ .modelName | ToPascal }} is an optimization {{ .modelName | ToLower }}.
type {{ .modelName | ToPascal }} struct {
	tableName struct{} `pg:"{{ .tableName }}"` //nolint: unused

	Base

	ProjectVersionID       uuid.UUID       `pg:"project_version_id"`
	ProjectVersion         *ProjectVersion `pg:"rel:has-one"`
}

// {{ .modelName | ToPascal }}Configurator is the {{ .modelName | ToLower }} configurator when creating a new {{ .modelName | ToLower }}.
type {{ .modelName | ToPascal }}Configurator func(*{{ .modelName | ToPascal }})

// New{{ .modelName | ToPascal }} creates a configured job.
func New{{ .modelName | ToPascal }}(opts ...{{ .modelName | ToPascal }}Configurator) *{{ .modelName | ToPascal }} {
	output := &{{ .modelName | ToPascal }}{}

	for _, opt := range opts {
		opt(output)
	}

	output.Link()

	return output
}

// Link implements the Linker interface.
func (s *{{ .modelName | ToPascal }}) Link() {
	if s.ProjectVersion != nil && s.ProjectVersion.ID != uuid.Nil {
		s.ProjectVersionID = s.ProjectVersion.ID
	}
}

// {{ .modelName | ToPascal }}WithID configures the identifier.
func {{ .modelName | ToPascal }}WithID(id uuid.UUID) {{ .modelName | ToPascal }}Configurator {
	return func({{ .modelName|ToLower }} *{{ .modelName | ToPascal }}) {
		{{ .modelName|ToLower }}.ID = id
	}
}

// {{ .modelName | ToPascal }}WithProjectVersion configures the project version.
func {{ .modelName | ToPascal }}WithProjectVersion(projectVersion *ProjectVersion) {{ .modelName | ToPascal }}Configurator {
	return func({{ .modelName | ToLower }} *{{ .modelName | ToPascal }}) {
		{{ .modelName | ToLower }}.ProjectVersion = projectVersion
	}
}

type Create{{ .modelName | ToPascal }}Request struct {
	ProjectVersionID uuid.UUID `json:"-"`
}

type List{{ .modelName | ToPascal }}Request struct {
	*PageRequest
	ProjectVersionID uuid.UUID
	SortField        string `validate:"oneof=created_at updated_at"`
}

func NewList{{ .modelName | ToPascal }}Request() *List{{ .modelName | ToPascal }}Request {
	listreq := &List{{ .modelName | ToPascal }}Request{
		PageRequest: NewPageRequest(),
	}
	listreq.SortField = "created_at"
	listreq.SortDirection = "desc"

	return listreq
}

// SetPagination implements the PageSetter interface.
func (r *List{{ .modelName | ToPascal }}Request) SetPagination(pagination *PageRequest) {
	r.PageRequest = pagination
}

// Values builds url.Values data set.
func (r *List{{ .modelName | ToPascal }}Request) Values() url.Values {
	output := make(url.Values)

	if r.PageRequest != nil {
		output = r.PageRequest.Values()
	}

	if r.SortField != "" {
		output.Set("sort_field", r.SortField)
	}

	return output
}

func (r *List{{ .modelName | ToPascal }}Request) Bind(binder *echo.ValueBinder) error {
	if err := r.PageRequest.Bind(binder); err != nil {
		return err
	}

	if err := binder.
		String("sort_field", &r.SortField).
		BindError(); err != nil {
		return err
	}

	return nil
}

type {{ .modelName | ToPascal }}UpdateRequest struct {}
